-- Ubercart RCCPagos --
Based on uc_sermepa

This module includes TPV for Credicoop bank that use rccpago secure server.

-- Install & Config --
Enable the module.
Add the data provided by your bank in 
Store administration > Configuration > Payment > Edit > Rccpagos settings
or admin/store/settings/payment/edit/uc_rccpago
Es necesario agregar un banco por forma de pago
solo funciona con moneda pesos
-- Roadmap --
Primera version

-- Collaborate --
If you want to help with this module, you can submit patches, find bugs and
communicate them through the issue queue or even your own rccpago module 
for a bank in particular

