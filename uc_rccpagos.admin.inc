<?php
// $Id: uc_rccpagos.admin.inc,v 1.1.2.3 2010/07/22 15:54:57 pcambra Exp $

/**
 * @file
 * Admin functions for uc rccpagos.
 */

/**
 * Display all the banks with its possible operations.
 */
function uc_rccpagos_admin_settings() {
  $banks = uc_rccpagos_get_all_banks();
  $rows = array();
  $output = "";
  foreach ($banks as $bank_details) {
    $row = array();
    $row[] = $bank_details['bankname'];
    $row[] = $bank_details['bankmode'];
    if (!$bank_details['enabled']) {
      $row[] = t('Disabled');
    }
    else {
      $row[] = t('Enabled');
    }
    $row[] = l(t('Edit'), 'admin/store/settings/uc_rccpago/edit/'. $bank_details['bankcode']);
    $row[] = l(t('Delete'), 'admin/store/settings/uc_rccpago/delete/'. $bank_details['bankcode']);
    $row[] = l(t('Export'), 'admin/store/settings/uc_rccpago/export/'. $bank_details['bankcode']);
    $rows[] = $row;
  }
  if (count($rows)) {
    $header = array(t('Bank Name'), t('Mode'), t('Enabled'), array('data' => t('Operations'), 'colspan' => 3));
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('There are no banks configured, you should start adding one.');
  }
  drupal_set_message(t('These are the banks you have available, you can also <a href="@addone">add one</a> or <a href="@importone">import one</a>',
    array('@addone' => url('admin/store/settings/uc_rccpago/add'), '@importone' => url('admin/store/settings/uc_rccpago/import'))));
  return $output;
}

/**
 * Form to manage the banks.
 */
function uc_rccpagos_form_bank($form_state, $mode = NULL) {
  $form = array();
  $bankcode = arg(5);
  //Determine the default values  
  $bank_defaults = new stdClass;
  $bank_defaults->enabled = 0;
  $bank_defaults->encryption = 'md5';
  $bank_defaults->bankmode = 'test';
  $bank_defaults->currency = 0;  
  if ($mode == 'edit' && $bankcode) {
    $bank_defaults = uc_rccpagos_get_bank($bankcode);
  }
  $form['bankcode'] = array(
    '#type' => 'textfield',
    '#description' => t('Codename for the bank, must be unique'),
    '#title' => t('Bank Code'),
    '#default_value' => $bank_defaults->bankcode,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  if ($bank_defaults->bankcode) {
    $form['bankcode']['#disabled'] = TRUE;
    $form['bankcode']['#value'] = $bank_defaults->bankcode;
  }
  $form['bankname'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank Name'),
    '#default_value' => $bank_defaults->bankname,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['bankmode'] = array(
    '#type' => 'radios',
    '#title' => t('Mode of the bank'),
    '#default_value' => $bank_defaults->bankmode,
    '#options' => array('test' => t('Test'), 'live' => t('Live')),
    '#size' => 10,
    '#maxlength' => 5,
    '#required' => TRUE,
  );
  $form['rccpagosEmpresa'] = array(
    '#type' => 'textfield',
    '#title' => t('rccpagosEmpresa'),
    '#default_value' => $bank_defaults->rccpagosEmpresa,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['rccpagosClave'] = array(
    '#type' => 'textfield',
    '#title' => t('rccpagosClave'),
    '#default_value' => $bank_defaults->rccpagosClave,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['rccpagosMedioDePago'] = array(
    '#type' => 'radios',
    '#title' => t('rccpagosMedioDePago'),
    '#default_value' => $bank_defaults->rccpagosMedioDePago,
    '#options' => array(
      '0' => t('Imprimo y Pago'),
      '1' => t('Cabal'),
      '2' => t('Visa'),
      '3' => t('Mastercard'),
      '4' => t('American Express'),
    ),
  );
  $form['rccpagosMoneda'] = array(
    '#type' => 'radios',
    '#title' => t('rccpagosMoneda'),
    '#default_value' => $bank_defaults->rccpagosMoneda,
    '#options' => array(
      '0' => t('Pesos'),
      '1' => t('Dollar'),
      '2' => t('Euro'),
    ),
    '#required' => TRUE,
  );
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank connect url'),
    '#default_value' => $bank_defaults->url,
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $bank_defaults->enabled,
    '#required' => TRUE,
  );  
  $form['encryption'] = array(
    '#type' => 'radios',
    '#title' => t('Method of encryption'),
    '#default_value' => $bank_defaults->encryption,
    '#options' => array(
      'md5' => t('MD5'),
    ),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#suffix' => l(t('Cancel'), 'admin/store/settings/payment/edit/uc_rccpago'),
  );          

  return $form;
}

/**
 * Submit for bank edit/add form.
 */
function uc_rccpagos_form_bank_submit($form, &$form_state) {
  uc_rccpagos_save_bank($form_state['values']);
  drupal_set_message(t('Bank saved correctly'));
  $form_state['redirect'] = 'admin/store/settings/payment/edit/uc_rccpago';  
}

/**
 * Handle the restore/delete bank confirmation form.
 */
function uc_rccpagos_form_restore_delete_bank($form_state, $mode = NULL) {
  $form = array();
  $bankcode = arg(5);
  $bank = uc_rccpagos_get_bank($bankcode);  
  $form['bankname'] = array('#type' => 'value', '#value' => $bank->bankname);
  $form['bankcode'] = array('#type' => 'value', '#value' => $bank->bankcode);
  $form['bankmode'] = array('#type' => 'value', '#value' => $bank->bankmode);
  $form['mode'] = array('#type' => 'value', '#value' => $mode);
  if ($mode == 'restoretodefault') {
    $message = t('Are you sure you want to restore the configuration of %bank?', array('%bank' => $bank->bankname));
    $button = t('Restore settings');
  }
  elseif ($mode == 'delete') {
    $message = t('Are you sure you want to delete %bank in %mode mode?', array('%bank' => $bank->bankname, '%mode' => $bank->bankmode));
    $button = t('Delete');
  }
  $caption .= '<p>'. t('This action cannot be undone.') .'</p>';
  return confirm_form($form, $message, 'admin/store/settings/payment/edit/uc_rccpago', $caption, $button);  
}

function uc_rccpagos_form_restore_delete_bank_submit($form, &$form_state) {
  if ($form_state['values']['mode'] == 'delete') {
    uc_rccpagos_delete_bank($form_state['values']['bankcode']);
    drupal_set_message(t('%bank in %mode mode deleted', array('%bank' => $form_state['values']['bankname'], '%mode' => $form_state['values']['bankmode'])));  
  }
  elseif ($form_state['values']['mode'] == 'restoretodefault') {
    $bank = uc_rccpagos_default_banks($form_state['values']['bankcode']);
    uc_rccpagos_delete_bank($form_state['values']['bankcode']);
    drupal_write_record('uc_rccpagos_settings', $bank);
    drupal_set_message(t('The default settings of %bankname have been restored', array('%bankname' => $form_state['values']['bankname'])));
  }
  $form_state['redirect'] = 'admin/store/settings/payment/edit/uc_rccpago';
}
